#pragma once

#include <string>

#include "src/optimization.h"

using namespace alglib;

class Profile {
private:

	std::string					m_File;
	real_2d_array				m_Constraints;
	integer_1d_array			m_ConstraintTypes;
	integer_1d_array			m_Ranks;

    double**					m_Data;
    double**					m_NormData;
    
    double                      m_DistMax;
    double                      m_PriceMax;
    
    double                      m_DistMean;
    double                      m_PriceMean;
    double                      m_DistStdDeviation;
    double                      m_PriceStdDeviation;
    
public:

								Profile();
								Profile(std::string file_path);
								~Profile();

	const std::string&			getFile() { return m_File; }
    double**					getData() { return m_Data; }
    double**					getNormData() { return m_NormData; } 
	const real_2d_array&		getConstraints() { return m_Constraints; }
	const integer_1d_array&		getConstraintTypes() { return m_ConstraintTypes; }
	const integer_1d_array&		getRanks() { return m_Ranks; }
	const double				getPriceMax() { return m_PriceMax; }
	const double				getDistMax() { return m_DistMax; }
	void						initFromFile(std::string file_path);

	void						refactorConstraints(int i);
};