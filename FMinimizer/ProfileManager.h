#pragma once

#include <vector>

class Profile;

class ProfileManager {

private:

	std::vector<Profile*>			m_Profiles;
	std::string						m_Dir;

public:

									ProfileManager(std::string dir);
									ProfileManager();
									~ProfileManager();

	const std::vector<Profile*>&	getProfiles() {
		return m_Profiles;
	}
	Profile*						getProfile(int i) {
		return m_Profiles[i];
	}
	const Profile*					getNext() {
		if (m_Profiles.size() == 0) return NULL;
		const Profile* ret = m_Profiles.back();
		m_Profiles.pop_back();
		return ret;
	}
	void							initFromDirectory(std::string dir);

};