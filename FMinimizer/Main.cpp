 #include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <dlib/svm.h>

#include "wingetopt.h"

#include "src/optimization.h"

#include "ProfileManager.h"
#include "Profile.h"

using namespace std;
using namespace dlib;
using namespace alglib;

void function1_grad(const real_1d_array &x, double &func, void *ptr)
{
	//
	// this callback calculates f(x0,x1) = 100*(x0+3)^4 + (x1-3)^4
	// and its derivatives df/d0 and df/dx1
	//
	func = 100 * pow(x[0] + 3, 4) + pow(x[1] - 3, 4);

	//std::cout << x[0] << std::endl;
	//std::cout << x[1] << std::endl;

	//grad[0] = 400 * pow(x[0] + 3, 3);
	//grad[1] = 4 * pow(x[1] - 3, 3);
}

void RankFunc(const real_1d_array &x, double &func, real_1d_array &grad, void *ptr)
{
	//
	// this callback calculates f(x0,x1) = 100*(x0+3)^4 + (x1-3)^4
	// and its derivatives df/d0 and df/dx1
	//
	ProfileManager* mgr = (ProfileManager*)ptr;
	//std::cout << "Pro Size " << mgr->getProfiles().size() << std::endl;
	//std::cout << "Size " << x.length() << std::endl;
	//std::cout << "Size " << x[0] << "  " << x[1] << std::endl;
	
	double weights = 0.5 * ((x[0] * x[0]) + (x[1] * x[1]));
	double slacks = 0;
	double C = 90;
	for (int i = 2; i < 23; i++) {
		slacks += x[i];
	}

	func = weights + C * slacks;
    
    grad[0] = x[0];
    grad[1] = x[1];
    for (int i = 2; i < 23; i++) {
        grad[i] = C;
    }
}

void CreateDataForSVMRank(ProfileManager* mgr);
void SVMRank(ProfileManager* mgr);
void CustomSMVRank (ProfileManager* mgr);

int main(int argc, char **argv)
{
	

	int opt;
	while ((opt = getopt(argc, argv, "dC:")) != -1)
		switch (opt)
	{
		case 'd':
			break;
		case 'C':
			break;
		case '?':
			if (optopt == 'C')
				fprintf(stderr, "Option -%c requires a constant value.\n", optopt);
			else if (isprint(optopt))
				fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf(stderr,
				"Unknown option character `\\x%x'.\n",
				optopt);
			return 1;
		default:
			abort();
	}
	/*
	std::vector<std::string>   result;
    std::string                line;
    std::getline(str,line);

    std::stringstream          lineStream(line);
    std::string                cell;

    while(std::getline(lineStream,cell,','))
    {
        result.push_back(cell);
    }
	*/
	
#ifdef __APPLE__
	ProfileManager* mgr = new ProfileManager("./profiles");
#else
    ProfileManager* mgr = new ProfileManager("./profiles");
#endif
    
    //CreateDataForSVMRank(mgr);
	//SVMRank(mgr);
    CustomSMVRank(mgr);
    
	system("PAUSE");

	return 0;
}

void CustomSMVRank (ProfileManager* mgr) {
    real_1d_array x = "[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]";
    real_1d_array start = "[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]";
    real_2d_array c = "[[1,0,2],[1,1,10]]";
    real_1d_array bndl = "[-INF,-INF,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]";
    real_1d_array bndu = "[+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF,+INF]";
    integer_1d_array ct = "[1,1]";
    
	ofstream out;
	out.open("custom_weights.txt");

    //
    // These variables define stopping conditions for the optimizer.
    //
    // We use very simple condition - |g|<=epsg
    //
    double epsg = 0.001;
    double epsf = 0;
    double epsx = 0;
    ae_int_t maxits = 0;
    
    //
    // Now we are ready to actually optimize something:
    // * first we create optimizer
    // * we add linear constraints
    // * we tune stopping conditions
    // * and, finally, optimize and obtain results...
    //
    
    //for (int i = 0; i < 1; i++) {
    for (int i = 0; i < 50; i++) {
		minbleicstate state;
		minbleicreport rep;

        real_1d_array start = "[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]";
        Profile* pro = mgr->getProfile(i);
        integer_1d_array ranks = pro->getRanks();
        //std::cout << "Index " << i << " " << pro->getFile() << std::endl;
        double** rawData = pro->getNormData();
        
        //minbleiccreatef(start, 0.001, state);
        minbleiccreate(start, state);
        minbleicsetlc(state, pro->getConstraints(), pro->getConstraintTypes(), 20);
        minbleicsetbc(state, bndl, bndu);
        //minbleicsetlc()
        minbleicsetcond(state, epsg, epsf, epsx, maxits);
        minbleicoptimize(state, RankFunc, NULL, mgr);
        minbleicresults(state, x, rep);
        //minbleicrestartfrom(state, start);
        //printf("%d\n", int(rep.terminationtype)); // EXPECTED: 4
        //printf("%s\n", x.tostring(2).c_str()); // EXPECTED: [2,4]
        
        //printf("Rank re     %g\n", rawData[i][0] * x[0] + rawData[i][1] * x[1]); // EXPECTED: 4
        // printf("Rank irr    %g\n", rawData[i][2] * x[0] + rawData[i][3] * x[1]); // EXPECTED: 4
        int correct = 0;
        for (int j = 0; j < 20; j++) {
            double rankB = rawData[j][0] * x[0] + rawData[j][1] * x[1];
            double rankA = rawData[j][2] * x[0] + rawData[j][3] * x[1];
            int pred = 0;
            if ( rankB > rankA )
                pred = 1;
            
            if ( pred == ranks[j] )
                correct++;
            
            //cout << "\nranking " << rawUnNormData[j][0] << " " << rawUnNormData[j][1] << " : " << pred << " where " << ranks[j] << endl;
            //cout << "\nranking " << rawUnNormData[j][2] << " " << rawUnNormData[j][3] << " : " << pred << endl;
        }
        
        cout << "Correct predictions for profile " << pro->getFile() << " : " << correct << " / 20 ";
        cout << " Weight : " << x.tostring(2).c_str() << std::endl;
        
		out << "Profile " << i << " [" << x[0] << " , " << x[1] << "]" << std::endl;
    }

	out.close();
}

void CreateDataForSVMRank(ProfileManager* mgr) {
    for (int i = 0; i < 50; i++) {
        Profile * pro = mgr->getProfile(i);
        double** rawData = pro->getNormData();
        integer_1d_array ranks = pro->getRanks();
        ofstream profTrainOut;
        std::string filePath = std::string("./traindata/") + "train_" + std::to_string(i) + std::string(".dat");
        profTrainOut.open(filePath);
        for (int j = 0; j < 20; j++) {
            int qid = j+1;
            if (ranks[j] == 1) {
                profTrainOut << 2 << " qid:" << qid << " 1:" << rawData[j][0] << " 2:" << rawData[j][1] << std::endl;
                profTrainOut << 1 << " qid:" << qid << " 1:" << rawData[j][2] << " 2:" << rawData[j][3] << std::endl;
            } else {
                profTrainOut << 1 << " qid:" << qid << " 1:" << rawData[j][0] << " 2:" << rawData[j][1] << std::endl;
                profTrainOut << 2 << " qid:" << qid << " 1:" << rawData[j][2] << " 2:" << rawData[j][3] << std::endl;
            }
            
        }
        profTrainOut.close();
    }
}

void TrainSVMRank(ProfileManager* mgr) {
    for (int i = 0; i < 50; i++) {
        Profile * pro = mgr->getProfile(i);
        double** rawData = pro->getNormData();
        integer_1d_array ranks = pro->getRanks();
        ofstream profTrainOut;
        std::string filePath = std::string("./traindata/") + "train_" + std::to_string(i) + std::string(".dat");
        profTrainOut.open(filePath);
        for (int j = 0; j < 20; j++) {
            int qid = j+1;
            if (ranks[j] == 1) {
                profTrainOut << 1 << " qid:" << qid << " 1:" << rawData[j][0] << " 2:" << rawData[j][1] << std::endl;
                profTrainOut << 2 << " qid:" << qid << " 1:" << rawData[j][2] << " 2:" << rawData[j][3] << std::endl;
            } else {
                profTrainOut << 2 << " qid:" << qid << " 1:" << rawData[j][0] << " 2:" << rawData[j][1] << std::endl;
                profTrainOut << 1 << " qid:" << qid << " 1:" << rawData[j][2] << " 2:" << rawData[j][3] << std::endl;
            }
            
        }
        profTrainOut.close();
    }
}

void SVMRank(ProfileManager* mgr) {
	typedef matrix<double, 2, 1> sample_type;

	// Now let's make some testing data.  To make it really simple, let's
	// suppose that vectors with positive values in the first dimension
	// should rank higher than other vectors.  So what we do is make
	// examples of relevant (i.e. high ranking) and non-relevant (i.e. low
	// ranking) vectors and store them into a ranking_pair object like so:

	for (int i = 0; i < 50; i++) {
		Profile * pro = mgr->getProfile(i);
        //std::cout << "File : " << pro->getFile() << std::endl;
		double dMax = pro->getDistMax();
		double pMax = pro->getPriceMax();
        double** rawData = pro->getNormData();
        double** rawUnNormData = pro->getData();
		integer_1d_array ranks = pro->getRanks();
		std::vector<ranking_pair<sample_type> > queries;
        
		for (double C = 0.01; C < 100.0f; C += 3) {
			int correct = 0;

			for (int lOut = 0; lOut < 20; lOut++) {

				queries.clear();

				for (int j = 0; j < 20; j++) {

					if (lOut == j)
						continue;

					ranking_pair<sample_type> data;
					sample_type sampA;
					sample_type sampB;
					sampA = rawData[j][0], rawData[j][1];
					sampB = rawData[j][2], rawData[j][3];
					//printf("Rank %d\n", ranks[j]);
					//printf("Price A %g Dist A %g\n", rawData[j][0], rawData[j][1]);
					//printf("Price B %g Dist B %g\n", rawData[j][2], rawData[j][3]);

					if (ranks[j] == 1) {
						data.relevant.push_back(sampB);
						data.nonrelevant.push_back(sampA);
					}
					else {
						data.relevant.push_back(sampA);
						data.nonrelevant.push_back(sampB);
					}

					queries.push_back(data);
				}

				typedef linear_kernel<sample_type> kernel_type;

				svm_rank_trainer<kernel_type> trainer;
				decision_function<kernel_type> rank;

				//std::cout << "This is C " << trainer.get_c() << std::endl;
				trainer.set_c(C);
				// We train just as before.  
				rank = trainer.train(queries);

				sample_type sampA;
				sample_type sampB;
				sampA = rawData[lOut][0] / pMax, rawData[lOut][1] / dMax;
				sampB = rawData[lOut][2] / pMax, rawData[lOut][3] / dMax;
				double rankB = rank(sampB);
				double rankA = rank(sampA);
				int pred = 0;
				if (rankB > rankA)
					pred = 1;

				if (pred == ranks[lOut]) {
					//std::cout << "Correct Prediction!!!\n";
					correct++;
				}
				else {
					//std::cout << "INcorrect Prediction!!!\n";
				}

			}
			std::cout << "Factor " << C << " ---- Correct   " << correct << "  !!!\n";
		}
		/*
        int correct = 0;
        for (int j = 0; j < 20; j++) {
            sample_type sampA;
            sample_type sampB;
            sampA = rawData[j][0], rawData[j][1];
            sampB = rawData[j][2], rawData[j][3];
            double rankB = rank(sampB);
            double rankA = rank(sampA);
            int pred = 0;
            if ( rankB > rankA )
                pred = 1;
            
            if ( pred == ranks[j] )
                correct++;
            
            //cout << "\nranking " << rawUnNormData[j][0] << " " << rawUnNormData[j][1] << " : " << pred << " where " << ranks[j] << endl;
            //cout << "\nranking " << rawUnNormData[j][2] << " " << rawUnNormData[j][3] << " : " << pred << endl;
        }
		*/
        //cout << "Correct predictions : " << correct << " / 20" << std::endl;
        
        //cout << "testing (ordering accuracy, mean average precision): " << test_ranking_function(rank, queries[3]) << endl;

        //cout << "Correct predictions for profile " << pro->getFile() << " : " << correct << " / 20 ";
        //cout << " Weights: [" << rank.basis_vectors(0)(0) << "," << rank.basis_vectors(0)(1) << "]" << endl;
        //cout << " Weights: \n" << rank.basis_vectors(0) << endl;
        
        //cout << "cross-validation (ordering accuracy, mean average precision): "
        //<< cross_validate_ranking_trainer(trainer, queries, 4) << endl;
        
        //cout << "learned ranking weights: \n" << rank.basis_vectors(0) << endl;
	}
}