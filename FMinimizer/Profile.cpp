#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include "Profile.h"

#define COL_COUNT 23
#define ROW_COUNT 20

Profile::Profile() {
	
}

Profile::Profile(std::string file_path) {
    m_DistMean = 0;
    m_PriceMean = 0;
    m_DistMax = 0;
    m_PriceMax= 0;
    m_DistStdDeviation= 0;
    m_PriceStdDeviation= 0;
    
	initFromFile(file_path);
}

static const int COL_MIN = 1;
static const int COL_MAX = -1;
static const int CLASS_IDX = 5;
static const int ROW_MIN = 1;

void Profile::initFromFile(std::string file_path) {
	std::ifstream stream(file_path);
	std::string line;

    m_File = file_path;
    
	// Skip first ROW_MIN lines
	for (int i = 0; i < ROW_MIN; i++ )
		std::getline(stream, line);

	std::vector< std::vector<std::string> > rows;
	while (std::getline(stream, line)) {
		std::vector<std::string> tokens;
		std::istringstream s(line);
		std::string field;
		//std::cout << line << std::endl;

		while (getline(s, field, ',')) {
			tokens.push_back(field);
		}

		rows.push_back(tokens);
	}

	int rowCount, colCount;
	rowCount = rows.size() ;
	if ( rowCount > 0 ) {
		colCount = rows[0].size() + COL_MAX - COL_MIN;
	}

	m_Data = new double*[rowCount];
	for (int i = 0; i < rowCount; i++) {
		m_Data[i] = new double[colCount];
	}
	for (int j = 0; j < rowCount; j++) {
		for (int i = 0; i < colCount; i++) {
			m_Data[j][i] = 0.0;
		}
	}
    m_NormData = new double*[rowCount];
    for (int i = 0; i < rowCount; i++) {
        m_NormData[i] = new double[colCount];
    }
    for (int j = 0; j < rowCount; j++) {
        for (int i = 0; i < colCount; i++) {
            m_NormData[j][i] = 0.0;
        }
    }
	m_Constraints.setlength(ROW_COUNT, COL_COUNT);
	m_ConstraintTypes.setlength(ROW_COUNT);
	m_Ranks.setlength(ROW_COUNT);

	for (int j = 0; j < rowCount; j++) {
		std::vector<std::string>& tokens = rows[j];
		m_Ranks[j] = std::stoi(tokens[CLASS_IDX]);
		int colIdx = 0;
		for (int i = COL_MIN; i < (tokens.size() + COL_MAX); i++) {
			//std::cout << tokens[i] << " | ";
			m_Data[j][colIdx] = std::stod(tokens[i]);
			colIdx++;
		}
	}

    // Find MAX values
    for (int j = 0; j < rowCount; j++) {
        if ( m_Data[j][0] > m_PriceMax ) m_PriceMax = m_Data[j][0];
        if ( m_Data[j][2] > m_PriceMax ) m_PriceMax = m_Data[j][2];
        if ( m_Data[j][1] > m_DistMax ) m_DistMax = m_Data[j][1];
        if ( m_Data[j][3] > m_DistMax ) m_DistMax = m_Data[j][3];
    }
    
    // Find MEAN values
    int total = 0;
    for (int j = 0; j < rowCount; j++) {
        m_PriceMean += m_Data[j][0];
        m_PriceMean += m_Data[j][2];
        m_DistMean += m_Data[j][1];
        m_DistMean += m_Data[j][3];
        total+=2;
    }
    
    m_PriceMean /= total;
    m_DistMean /= total;
    
    
    // Find data DEVIATIONS
    double* tmpPriceDevs = new double[total];
    double* tmpDistDevs = new double[total];
    double priceDevSum = 0;
    double distDevSum = 0;
    int count = 0;
    total = 0;
    for (int j = 0; j < rowCount; j++) {
        tmpPriceDevs[count] = ( m_Data[j][0] - m_PriceMean ) * ( m_Data[j][0] - m_PriceMean );
        tmpPriceDevs[count + 1] = ( m_Data[j][2] - m_PriceMean ) * ( m_Data[j][2] - m_PriceMean );
        tmpDistDevs[count] = ( m_Data[j][1] - m_DistMean ) * ( m_Data[j][1] - m_DistMean );
        tmpDistDevs[count + 1] = ( m_Data[j][3] - m_DistMean ) * ( m_Data[j][3] - m_DistMean );
        
        priceDevSum += tmpPriceDevs[count + 1] + tmpPriceDevs[count];
        distDevSum += tmpDistDevs[count + 1] + tmpDistDevs[count];
        
        count += 2;
        total += 2;
    }
    
    m_DistStdDeviation = sqrt(distDevSum / (total - 1));
    m_PriceStdDeviation = sqrt(priceDevSum / (total - 1));
    
    printf("Max Dist %g\n", m_DistMax);
    printf("Max Price %g\n", m_PriceMax);
    printf("Mean Dist %g\n", m_DistMean);
    printf("Mean Price %g\n", m_PriceMean);
    printf("StdDev Dist %g\n", m_DistStdDeviation);
    printf("StdDev Price %g\n", m_PriceStdDeviation);
    
    
    // Normalization
    for (int j = 0; j < rowCount; j++) {
        m_NormData[j][0] = m_Data[j][0];
        m_NormData[j][2] = m_Data[j][2];
        m_NormData[j][1] = m_Data[j][1];
        m_NormData[j][3] = m_Data[j][3];
        
		/*
        m_NormData[j][0] = m_Data[j][0] / m_PriceMax;
        m_NormData[j][2] = m_Data[j][2] / m_PriceMax;
        m_NormData[j][1] = m_Data[j][1] / m_DistMax;
        m_NormData[j][3] = m_Data[j][3] / m_DistMax;
        
		
        m_NormData[j][0] = (m_Data[j][0] - m_PriceMean) / ( 2 * m_PriceStdDeviation );
        m_NormData[j][2] = (m_Data[j][2] - m_PriceMean) / ( 2 * m_PriceStdDeviation );
        m_NormData[j][1] = (m_Data[j][1] - m_DistMean) / ( 2 * m_DistStdDeviation );
        m_NormData[j][3] = (m_Data[j][3] - m_DistMean) / ( 2 * m_DistStdDeviation );
        */
    }

	for (int j = 0; j < ROW_COUNT; j++) {
		for (int i = 0; i < COL_COUNT; i++) {
			m_Constraints[j][i] = 0;
		}
	}

	for (int j = 0; j < ROW_COUNT; j++) {
		if (m_Ranks[j] == 1) {
			m_Constraints[j][0] = m_NormData[j][2] - m_NormData[j][0];
			m_Constraints[j][1] = m_NormData[j][3] - m_NormData[j][1];
		} else {
			m_Constraints[j][0] = m_NormData[j][0] - m_NormData[j][2];
			m_Constraints[j][1] = m_NormData[j][1] - m_NormData[j][3];
		}
		//std::cout << "Rank " << m_Ranks[j] << std::endl;
	}

	for (int j = 2; j < ( COL_COUNT - 1 ); j++) {
		m_Constraints[j - 2][j] = -1.0;
	}

	for (int j = 0; j < ROW_COUNT; j++) {
		m_Constraints[j][COL_COUNT - 1] = -1.0;
		m_ConstraintTypes[j] = -1;
	}

	//std::cout << "ROWS " << rowCount << " COLS : " << colCount << std::endl;
	for (int j = 0; j < ROW_COUNT; j++) {
		std::cout << "Rank " << m_Ranks[j] << std::endl;
		for (int i = 0; i < COL_COUNT; i++) {
			std::cout << m_Constraints[j][i] << "|";
		}
		std::cout << "Type " << m_ConstraintTypes[j] << std::endl;
	}
}