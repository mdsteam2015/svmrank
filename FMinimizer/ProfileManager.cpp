#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>

#include "ProfileManager.h"
#include "Profile.h"

#ifdef _WIN32
#include "src/dirent.h"
#else
#include <sys/types.h>
#include <sys/dir.h>
#endif

ProfileManager::ProfileManager(std::string dir){
	initFromDirectory(dir);
}

ProfileManager::ProfileManager(){

}

static bool stringCompareDesc( const std::string &left, const std::string &right ){
    int ia, ib;
    
    sscanf(left.c_str(), "profile_%d.csv", &ia);
    sscanf(right.c_str(), "profile_%d.csv", &ib);
    
    std::cout << ia << std::endl;
    std::cout << ib << std::endl;
    
    if ( ia > ib )
        return true;
    else
        return false;
    
}

static bool stringCompareAsc( const std::string &left, const std::string &right ){
    int ia, ib;
    
    sscanf(left.c_str(), "profile_%d.csv", &ia);
    sscanf(right.c_str(), "profile_%d.csv", &ib);
    
    std::cout << ia << std::endl;
    std::cout << ib << std::endl;
    
    if ( ia < ib )
        return true;
    else
        return false;
    
}

void ProfileManager::initFromDirectory(std::string dir_path) {
	DIR *dir;
	struct dirent *ent;
	m_Dir = dir_path;
    std::vector<std::string> files;
	if ( ( dir = opendir( dir_path.c_str() ) ) != NULL ) {
		// print all the files and directories within directory
      
		
        while ( ( ent = readdir(dir) ) != NULL) {
			if ( ( strcmp(ent->d_name, "." ) != 0 ) && ( strcmp(ent->d_name, ".." ) != 0 ) ) {
                files.push_back(ent->d_name);
			}
		}
        
        std::sort(files.begin(), files.end(), stringCompareAsc);
		closedir(dir);
        for (int i = 0; i < files.size(); i++ ) {
            std::cout << "File : " << files[i] << std::endl;;
            Profile* prof = new Profile(m_Dir + "/" + files[i]);
            m_Profiles.push_back(prof);
        }
	}
	else {
		// could not open directory
		std::cout << "Could not open directory" << std::endl;
	}

}